using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fight : MonoBehaviour
{

    public AK.Wwise.Event sword_attack;
    public AK.Wwise.Event sword_attack_final;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void attack()
    {
        sword_attack.Post(gameObject);
    }

    void attack_final()
    {
        sword_attack_final.Post(gameObject);
    }

}
