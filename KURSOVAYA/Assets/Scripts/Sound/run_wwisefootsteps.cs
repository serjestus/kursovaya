using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class run_wwisefootsteps : MonoBehaviour
{

    bool CanWalk = true;
    bool CanRun = true;

    public AK.Wwise.Event run_footstepsevent; // Переменная для хранения Wwise-ивента
    public AK.Wwise.Event sprint_footstepsevent; // Переменная для хранения Wwise-ивента
    public AK.Wwise.Event cloth_run_event;
    public AK.Wwise.Event cloth_sprint_event;
    public AK.Wwise.Event jump_event;
    public AK.Wwise.Event land_event;
    public AK.Wwise.Event roll_event;

    private vThirdPersonInput tpInput; // Переменная для обращения к магнитуде
    public LayerMask lm; // Выбор лейермаски (делается в компоненте)
    public GameObject leftfoot;
    public GameObject rightfoot;
    public GameObject spine;

    void Start()
    {
        tpInput = gameObject.GetComponent<vThirdPersonInput>(); // Делаем ссылку на компонент, который находится на контроллере
    }

    void cloth()
    {
        cloth_run_event.Post(spine);
    }

  //JUMP

    void jump()
    {
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 1f, lm))
        {
            AkSoundEngine.SetSwitch("SWITCH_surface_type", hit.collider.tag, gameObject);
            jump_event.Post(gameObject);
            Debug.Log(hit.collider.tag);
        }
    }


    //ROLL


    void roll()
    {
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 1f, lm))
        {
            AkSoundEngine.SetSwitch("SWITCH_surface_type", hit.collider.tag, gameObject);
            roll_event.Post(gameObject);
            Debug.Log(hit.collider.tag);
        }
    }


    void land()
    {
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 1f, lm))
        {
            AkSoundEngine.SetSwitch("SWITCH_surface_type", hit.collider.tag, gameObject);
            land_event.Post(gameObject);
            Debug.Log(hit.collider.tag);
        }
    }


    void cloth_sprint()
    {
        cloth_sprint_event.Post(spine); 
    }



     

    void footsteps(string noga) // принимаем из Animation Event в функции строчную переменную right или left и передаем ее в переменную noga
    {
        

        //WALK

        if (tpInput.cc.inputMagnitude > 0.1 && tpInput.cc.inputMagnitude <= 0.5) // Проверяем магнитуду
        {
            

            if (noga == "right") //если вызвали функцию с аргументом right
            {
                Run_PlayShagi(rightfoot); // Запускаем функцию на объекте правой ноги 
            }
            else if (noga == "left") // то же самое для левой ноги
            {
                Run_PlayShagi(leftfoot); // Запускаем функцию на объекте левой ноги
            }
        }

        //SPRINT

        else if (tpInput.cc.inputMagnitude > 0.5) // Проверяем магнитуду
        {
            if (noga == "right") //если вызвали функцию с аргументом right
            {
                Sprint_PlayShagi(rightfoot); // Запускаем функцию на объекте правой ноги 
            }
            else if (noga == "left") // то же самое для левой ноги
            {
                Sprint_PlayShagi(leftfoot); // Запускаем функцию на объекте левой ноги
            }
        }


        void Run_PlayShagi(GameObject nogaObject) // функция проверки поверхности для нужного геймобъекта
        {
            if (Physics.Raycast(nogaObject.transform.position, Vector3.down, out RaycastHit hit, 0.3f, lm)) // Кидаем луч вниз и проверяем рейкастом поверхность
            {
                Debug.Log(hit.collider.tag); // В дебаге отображаем что за поверхность под ногами
                AkSoundEngine.SetSwitch("SWITCH_surface_type", hit.collider.tag, gameObject); // Выставляем свитч в свитчгруппе SWITCH_surface_type в положение поверхности,
                                                                                              // котороая под ногами. На занятии была ошибка с тем, что скопировал свитч с пробелом и такие тэги поверхности с делал!
                run_footstepsevent.Post(gameObject); // Запускаем ивент на геймобъекте nogaObject - левая ступня или правая
            }
        }

        void Sprint_PlayShagi(GameObject nogaObject) // функция проверки поверхности для нужного геймобъекта
        {
            if (Physics.Raycast(nogaObject.transform.position, Vector3.down, out RaycastHit hit, 0.2f, lm)) // Кидаем луч вниз и проверяем рейкастом поверхность
            {
                Debug.Log(hit.collider.tag); // В дебаге отображаем что за поверхность под ногами
                AkSoundEngine.SetSwitch("SWITCH_surface_type", hit.collider.tag, gameObject); // Выставляем свитч в свитчгруппе SWITCH_surface_type в положение поверхности,
                                                                                              // котороая под ногами. На занятии была ошибка с тем, что скопировал свитч с пробелом и такие тэги поверхности с делал!
                sprint_footstepsevent.Post(gameObject); // Запускаем ивент на геймобъекте nogaObject - левая ступня или правая
            }
        }

    }
}
