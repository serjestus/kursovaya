using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using Invector.PlayerController;
public class low_stamina : MonoBehaviour
{ 
    public vThirdPersonController control;
    public bool IsBreathing = false;
    public bool CanBreath = true;
    public AK.Wwise.Event breath_start_event;
    public AK.Wwise.Event breath_stop_event;
    

    // Start is called before the first frame update

    void Start()
    {
        AkSoundEngine.SetState("STATE_Stamina", "STATE_HighStamina");
    }
    void Update()
    {
        if (control.currentStamina <= 30)
        {
            if(CanBreath == true)
            {
                StaminaStateStart();
                CanBreath = false;
            }  
        }
        else if(control.currentStamina > 30)
        {
            StaminaStateStop();
            CanBreath = true;
        }

    }

    void StaminaStateStart()
    {
        
                AkSoundEngine.SetState("STATE_Stamina", "STATE_LowStamina");
                breath_start_event.Post(gameObject);
                IsBreathing = true;
                
    }
    void StaminaStateStop()
    {
        
                AkSoundEngine.SetState("STATE_Stamina", "STATE_HighStamina");
                breath_stop_event.Post(gameObject);
                IsBreathing = false;
         
    }
}
