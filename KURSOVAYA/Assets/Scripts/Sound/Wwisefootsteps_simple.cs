using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;


public class Wwisefootsteps_simple : MonoBehaviour
{

    public AK.Wwise.Event walkingEvent;
    public AK.Wwise.Event runningEvent;
    public LayerMask lMask;
    private vThirdPersonInput tpInput;
    private string surface;
    private vThirdPersonController tpController;
    
    
    // Start is called before the first frame update
    void Start()
    {
        tpInput = gameObject.GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();
    }

    

    void footstep()
    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {
            SurfaceCheck();
            
            {
                if (tpController.isSprinting)
                {
                    AkSoundEngine.SetSwitch("SWITCH_surface_type", surface, gameObject);
                    runningEvent.Post(gameObject);
                }
                
                else
                {
                    AkSoundEngine.SetSwitch("SWITCH_surface_type", surface, gameObject);
                    walkingEvent.Post(gameObject);
                }
            }
              
        } 
        
    }

    void SurfaceCheck()
    {
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 1f, lMask))
        {
            Debug.Log(hit.collider.tag);
            surface = hit.collider.tag;
        }
    }
    
}


