using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector;
using Invector.vCharacterController.AI;

public class BOSS : MonoBehaviour
{

    public AK.Wwise.Event footstep_event;
    public LayerMask lm;

    public vControlAIMelee enemyController;
    public AK.Wwise.Event COMEHERE_event;
    bool CanTalk = true;

    public AK.Wwise.Event Attack_event;

    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (enemyController.isInCombat == true)
        {
            if(CanTalk == true)
            {
                COMEHERE_event.Post(gameObject);
                CanTalk = false;
            }
        }

    }

    void footstep()
    {
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 1f, lm))
        {
            AkSoundEngine.SetSwitch("SWITCH_surface_type", hit.collider.tag, gameObject);
            footstep_event.Post(gameObject);
            Debug.Log(hit.collider.tag);
        }
    }

    void land()
    {
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 1f, lm))
        {
            AkSoundEngine.SetSwitch("SWITCH_surface_type", hit.collider.tag, gameObject);
            footstep_event.Post(gameObject);
            Debug.Log(hit.collider.tag);
        }
    }

    void boss_hit()
    {
        Attack_event.Post(gameObject);
    }
}
