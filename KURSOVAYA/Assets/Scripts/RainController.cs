using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainController : MonoBehaviour
{

    public GameObject rain;
    

    // Start is called before the first frame update
    void Start()
    {
        rain.SetActive(false);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        
        rain.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        
        
    }

}
