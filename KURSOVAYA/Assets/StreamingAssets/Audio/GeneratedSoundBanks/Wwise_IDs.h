/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_BIGFIRE1 = 2135458909U;
        static const AkUniqueID PLAY_BIGFIRE2 = 2135458910U;
        static const AkUniqueID PLAY_BIGFIRE3 = 2135458911U;
        static const AkUniqueID PLAY_BIGFIRE4 = 2135458904U;
        static const AkUniqueID PLAY_BIGFIRE5 = 2135458905U;
        static const AkUniqueID PLAY_BIGFIRE6 = 2135458906U;
        static const AkUniqueID PLAY_BOSS_COMEHERE = 112486458U;
        static const AkUniqueID PLAY_CATANA_WHOOSH_FINAL_ATTACK = 1758260183U;
        static const AkUniqueID PLAY_CATANA_WHOOSH_WEAK_ATTACK = 2478160983U;
        static const AkUniqueID PLAY_CITIZEN_HELLO = 3238542771U;
        static const AkUniqueID PLAY_CLOTHFORRUN = 857401310U;
        static const AkUniqueID PLAY_CLOTHFORSPRINT = 817724199U;
        static const AkUniqueID PLAY_CORRIDOR = 1530724216U;
        static const AkUniqueID PLAY_CORRIDORRAIN1 = 1566130503U;
        static const AkUniqueID PLAY_CORRIDORRAIN2 = 1566130500U;
        static const AkUniqueID PLAY_DEATH = 1172822028U;
        static const AkUniqueID PLAY_DOG = 2992249820U;
        static const AkUniqueID PLAY_DOG1 = 2681854821U;
        static const AkUniqueID PLAY_DOG2 = 2681854822U;
        static const AkUniqueID PLAY_FINAL_ATTACK = 826855301U;
        static const AkUniqueID PLAY_FOREST = 207755397U;
        static const AkUniqueID PLAY_GOOD_BOY = 460392834U;
        static const AkUniqueID PLAY_HEAVY_BREATHING = 409273600U;
        static const AkUniqueID PLAY_HITS = 2855285396U;
        static const AkUniqueID PLAY_HORSE_EATING = 286106790U;
        static const AkUniqueID PLAY_HORSE_EATING1 = 2121937251U;
        static const AkUniqueID PLAY_HORSE_EATING2 = 2121937248U;
        static const AkUniqueID PLAY_HORSE_EATING3 = 2121937249U;
        static const AkUniqueID PLAY_JUMP_SURFACE_SWITCH = 3681686353U;
        static const AkUniqueID PLAY_LAKE = 40648359U;
        static const AkUniqueID PLAY_LAKE1 = 2003214548U;
        static const AkUniqueID PLAY_LAND_SURFACE_SWITCH = 976690416U;
        static const AkUniqueID PLAY_LOCOMOTION = 112612293U;
        static const AkUniqueID PLAY_MOUNTAIN = 825230623U;
        static const AkUniqueID PLAY_MUSIC_SWITCH = 1228139402U;
        static const AkUniqueID PLAY_MUSIC_SWITCH_01 = 3426349064U;
        static const AkUniqueID PLAY_RECEIVE_DAMAGE = 4018850759U;
        static const AkUniqueID PLAY_ROLL_SURFACE_SWITCH = 1418754070U;
        static const AkUniqueID PLAY_RUN_SURFACE_SWITCH = 3020648648U;
        static const AkUniqueID PLAY_SELLER = 1144785939U;
        static const AkUniqueID PLAY_SENSEI_TRAINING = 1464035170U;
        static const AkUniqueID PLAY_SPRINT_SURFACE_SWITCH = 2989823321U;
        static const AkUniqueID PLAY_TALKING_SECURITY_1 = 4227432877U;
        static const AkUniqueID PLAY_TALKING_SECURITY_2 = 4227432878U;
        static const AkUniqueID PLAY_TORCHFIRE1 = 1422837333U;
        static const AkUniqueID PLAY_TORCHFIRE2 = 1422837334U;
        static const AkUniqueID PLAY_VILLAGE = 3399607102U;
        static const AkUniqueID PLAY_WHISTLER = 568018294U;
        static const AkUniqueID PLAY_WOMAN = 2295360992U;
        static const AkUniqueID STOP_HEAVY_BREATHING = 3964894194U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace STATE_CROUCH
        {
            static const AkUniqueID GROUP = 2250279633U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STATE_CROUCH = 2250279633U;
                static const AkUniqueID STATE_NOCROUCH = 724965512U;
            } // namespace STATE
        } // namespace STATE_CROUCH

        namespace STATE_HEALTH
        {
            static const AkUniqueID GROUP = 735325645U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STATE_HIGHHEALTH = 2617670485U;
                static const AkUniqueID STATE_LOWHEALTH = 102563465U;
            } // namespace STATE
        } // namespace STATE_HEALTH

        namespace STATE_LOCATION
        {
            static const AkUniqueID GROUP = 811758810U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STATE_CORRIDOR = 2322225605U;
                static const AkUniqueID STATE_FOREST = 4216640728U;
                static const AkUniqueID STATE_MOUNTAIN = 1996746542U;
                static const AkUniqueID STATE_VILLAGE = 1091089913U;
            } // namespace STATE
        } // namespace STATE_LOCATION

        namespace STATE_MUSIC
        {
            static const AkUniqueID GROUP = 1992488372U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STATE_BATTLE = 968190373U;
                static const AkUniqueID STATE_EXPLORATION = 3594033046U;
                static const AkUniqueID STATE_VILLAGE = 1091089913U;
            } // namespace STATE
        } // namespace STATE_MUSIC

        namespace STATE_RAIN
        {
            static const AkUniqueID GROUP = 2636934089U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STATE_RAINOFF = 855091576U;
                static const AkUniqueID STATE_RAINON = 3911671074U;
            } // namespace STATE
        } // namespace STATE_RAIN

        namespace STATE_STAMINA
        {
            static const AkUniqueID GROUP = 2108259450U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STATE_HIGHSTAMINA = 3441461362U;
                static const AkUniqueID STATE_LOWSTAMINA = 489333646U;
            } // namespace STATE
        } // namespace STATE_STAMINA

    } // namespace STATES

    namespace SWITCHES
    {
        namespace SWITCH_SURFACE_TYPE
        {
            static const AkUniqueID GROUP = 2818941756U;

            namespace SWITCH
            {
                static const AkUniqueID SWITCH_SURFACE_GRASS = 3949292034U;
                static const AkUniqueID SWITCH_SURFACE_ROCK = 1520024863U;
                static const AkUniqueID SWITCH_SURFACE_SAND = 3129684586U;
            } // namespace SWITCH
        } // namespace SWITCH_SURFACE_TYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID AZIMUTH = 1437246667U;
        static const AkUniqueID EXT_CAMERA_HEIGHT = 1862344870U;
        static const AkUniqueID RTPC_OCCLUSION = 1664992182U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID ENEMIES = 2242381963U;
        static const AkUniqueID HEARTBEAT = 2179486487U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID PLAYER = 1069431850U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID CORRIDOR_REVERB_01 = 3535232184U;
        static const AkUniqueID CORRIDOR_REVERB_02 = 3535232187U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
